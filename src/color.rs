use crate::vec3::Vec3;

pub fn write_color(color: Vec3) {
    println!(
        "{} {} {}\n",
        (255.999 * color.x) as u32,
        (255.999 * color.y) as u32,
        (255.999 * color.z) as u32
    )
}
