use std::ops::{Add, Div, Mul, Sub};

#[derive(Copy, Clone)]
pub struct Vec3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vec3 {
    pub fn new(x: f64, y: f64, z: f64) -> Vec3 {
        Vec3 { x, y, z }
    }

    pub fn len(&self) -> f64 {
        (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
    }

    pub fn unit_vector(v: Vec3) -> Vec3 {
        let lenght = v.len();
        v.div(lenght)
    }

    pub fn dot(v1: Vec3, v2: Vec3) -> f64 {
        return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    }
}

impl Add for Vec3 {
    type Output = Vec3;

    fn add(self, other: Vec3) -> Vec3 {
        Vec3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl Div<f64> for &Vec3 {
    type Output = Vec3;

    fn div(self, t: f64) -> Vec3 {
        (1.0 / t) * self
    }
}

impl Sub<Vec3> for &Vec3 {
    type Output = Vec3;

    fn sub(self, vec2: Vec3) -> Vec3 {
        Vec3 {
            x: self.x - vec2.x,
            y: self.y - vec2.y,
            z: self.z - vec2.z,
        }
    }
}

impl Sub<Vec3> for Vec3 {
    type Output = Vec3;

    fn sub(self, vec2: Vec3) -> Vec3 {
        Vec3 {
            x: self.x - vec2.x,
            y: self.y - vec2.y,
            z: self.z - vec2.z,
        }
    }
}

impl Mul<Vec3> for &Vec3 {
    type Output = Vec3;

    fn mul(self, vec2: Vec3) -> Vec3 {
        Vec3 {
            x: self.x * vec2.x,
            y: self.y * vec2.y,
            z: self.z * vec2.z,
        }
    }
}

impl Mul<&Vec3> for f64 {
    type Output = Vec3;

    fn mul(self, vec2: &Vec3) -> Vec3 {
        Vec3 {
            x: self * vec2.x,
            y: self * vec2.y,
            z: self * vec2.z,
        }
    }
}

impl Mul<f64> for &Vec3 {
    type Output = Vec3;

    fn mul(self, t: f64) -> Vec3 {
        Vec3 {
            x: t * self.x,
            y: t * self.y,
            z: t * self.z,
        }
    }
}
