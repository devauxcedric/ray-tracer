mod color;
mod ray;
mod vec3;

use ray::Ray;
use vec3::Vec3;
use vec3::Vec3 as Color;
use vec3::Vec3 as Point;

fn ray_color(ray: Ray) -> Color {
    let t = hit_sphere(Point::new(0.0, 0.0, -1.0), 0.5, &ray);
    if t > 0.0 {
        let N = Vec3::unit_vector(ray.at(t) - Vec3::new(0.0, 0.0, -1.0));
        return 0.5 * &Color::new(N.x + 1.0, N.y + 1.0, N.z + 1.0);
    }
    let unit_direction = Vec3::unit_vector(ray.direction);
    let t2 = 0.5 * (unit_direction.y + 1.0);
    (1.0 - t2) * &Color::new(1.0, 1.0, 1.0) + t2 * &Color::new(0.5, 0.7, 1.0)
}

fn hit_sphere(center: Point, radius: f64, r: &Ray) -> f64 {
    let oc = r.origin - center;
    let a = Vec3::dot(r.direction, r.direction);
    let b = 2.0 * Vec3::dot(oc, r.direction);
    let c = Vec3::dot(oc, oc) - (radius * radius);
    let discriminant = (b * b) - (4.0 * a * c);
    if discriminant < 0.0 {
        return -1.0;
    } else {
        return (-b - discriminant.sqrt()) / (2.0 * a);
    }
}

fn main() {
    // Image
    let aspect_ratio = 16.0 / 9.0;
    let image_width = 400;
    let image_height = (image_width as f64 / aspect_ratio) as u32;

    let viewport_height = 2.0;
    let viewport_width = aspect_ratio * viewport_height;
    let focal_length = 1.0;

    let origin = Vec3::new(0.0, 0.0, 0.0);
    let horizontal = Vec3::new(viewport_width, 0.0, 0.0);
    let vertical = Vec3::new(0.0, viewport_width, 0.0);
    let lower_left_corner =
        &origin - (&horizontal / 2.0) - (&vertical / 2.0) - Vec3::new(0.0, 0.0, focal_length);

    println!("P3\n{} {}\n255\n", image_width, image_height);

    for j in (0..image_height).rev() {
        eprintln!("Lignes restantes: {}\n", j);
        for i in 0..(image_width as u32) {
            let u = (i as f64) / (image_width - 1) as f64;
            let v = (j as f64) / (image_height as f64 - 1.0);

            let r = Ray::new(
                origin,
                lower_left_corner + u * &horizontal + v * &vertical - origin,
            );
            let pixel_color = ray_color(r);

            color::write_color(pixel_color);
        }
    }
    eprintln!("Fini\n");
}
